'use client'

import { prepareFundNewUTxOTx } from "andamiojs";
import { andamioConfig } from "../../config";
import { useWallet } from "@meshsdk/react";

const FundNewUTxO = () => {
    const { wallet, connected } = useWallet();
    if (connected) {
      const handleClick = async () => {
        const FUND_NEW_UTXO_TX = await prepareFundNewUTxOTx(
          wallet,
          andamioConfig,
          1000,
          150
        );
        const res = await FUND_NEW_UTXO_TX.runTx();
        console.log(res);
      };
      return (
        <button className="button-1" onClick={handleClick}>
          Add New Fund UTxO
        </button>
      );
    }
    return <>Wallet not connected</>;
  };
  
  export default FundNewUTxO;
  