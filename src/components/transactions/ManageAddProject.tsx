import { useWallet } from "@meshsdk/react";
import { ProjectData, prepareManageAddProjectTx } from "andamiojs";
import { andamioConfig } from "../../config";
import { ChangeEvent, useState } from "react";

const ManageAddProject = (props: { closeModal: () => void }) => {
  const [formData, setFormData] = useState({
    contractToken: "",
    projectHashId: "",
    expirationTime: 0,
    lovelaceAmount: 0,
    projectTokenAmount: 0,
  });

  const handleInputChange = (e: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]:
        name === "expirationTime" ||
        name === "lovelaceAmount" ||
        name === "projectTokenAmount"
          ? parseInt(value, 10)
          : value,
    });
  };

  let project: ProjectData;
  const { wallet, connected } = useWallet();
  if (connected) {
    const handleClick = async () => {
      project = formData;
      console.log(JSON.stringify(project, null, 4));
      const MANAGE_ADD_PROJECT_TX = await prepareManageAddProjectTx(
        wallet,
        project,
        andamioConfig
      );
      const res = await MANAGE_ADD_PROJECT_TX.runTx();
      console.log(res);
      props.closeModal();
    };
    return (
      <div className="flex flex-col py-10 items-center bg-gradient-br w-max p-24">
        <div className="font-extrabold mb-4 text-xl">Selected Project</div>
        <div>
          <form>
            <table>
              <tr>
                <td>Contract Token:</td>
                <td className="p-4">
                  <input
                    type="text"
                    name="contractToken"
                    value={formData.contractToken}
                    onChange={handleInputChange}
                    className="bg-slate-700 p-2 rounded-md font-extrabold"
                  />
                </td>
              </tr>
              <tr>
                <td>Project Hash Id:</td>
                <td className="p-4">
                  <input
                    type="text"
                    name="projectHashId"
                    value={formData.projectHashId}
                    onChange={handleInputChange}
                    className="bg-slate-700 p-2 rounded-md font-extrabold"
                  />
                </td>
              </tr>
              <tr>
                <td>Expiration Time:</td>
                <td className="p-4">
                  <input
                    type="number"
                    name="expirationTime"
                    value={formData.expirationTime}
                    onChange={handleInputChange}
                    className="bg-slate-700 p-2 rounded-md font-extrabold"
                  />
                </td>
              </tr>
              <tr>
                <td>Lovelace Amount:</td>
                <td className="p-4">
                  <input
                    type="number"
                    name="lovelaceAmount"
                    value={formData.lovelaceAmount}
                    onChange={handleInputChange}
                    className="bg-slate-700 p-2 rounded-md font-extrabold"
                  />
                </td>
              </tr>
              <tr>
                <td>Project Token Amount:</td>
                <td className="p-4">
                  <input
                    type="number"
                    name="projectTokenAmount"
                    value={formData.projectTokenAmount}
                    onChange={handleInputChange}
                    className="bg-slate-700 p-2 rounded-md font-extrabold"
                  />
                </td>
              </tr>
            </table>
          </form>
        </div>
        <button
          onClick={handleClick}
          className="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded w-fit mt-6"
          type="button"
        >
          Confirm Add Project
        </button>
      </div>
    );
  }
  return <>Wallet not connected</>;
};

export default ManageAddProject;
