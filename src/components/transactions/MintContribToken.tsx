import { useWallet } from "@meshsdk/react";
import { MintContribTokenTx, prepareMintContribTokenTx } from "andamiojs";
import { andamioConfig } from "../../config";

const MintContribToken = (props: { tokenAlias: string }) => {
  const { wallet, connected } = useWallet();
  if (connected) {
    const handleClick = async () => {
      const MINT_CONTRIB_TOKEN_PAIR_TX = await prepareMintContribTokenTx(
        wallet,
        props.tokenAlias,
        andamioConfig
      );
      const res = await MINT_CONTRIB_TOKEN_PAIR_TX.runTx();
      console.log(res);
    };
    return (
      <button className="button-1" onClick={handleClick}>
        Confirm Minting Tx
      </button>
    );
  }
  return <>Wallet not connected</>;
};

export default MintContribToken;
