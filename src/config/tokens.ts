export const tokens = {
  contributorPolicyID:
    'f3e6f19d47103fc04f794ca1eaa786171f00446cb8c9a5a6939fe8a1',
  adminAsset:
    '3ef6cc35883f162e7ab1e96f633958a8cb42c8fd33ad2e8d9634019a61646d696e312d6e6674',
  adminPolicyID: '3ef6cc35883f162e7ab1e96f633958a8cb42c8fd33ad2e8d9634019a',
  adminTokenName: '61646d696e312d6e6674',
  projectAsset:
    'fb45417ab92a155da3b31a8928c873eb9fd36c62184c736f189d334c7447696d62616c',
  projectTokenPolicyID:
    'fb45417ab92a155da3b31a8928c873eb9fd36c62184c736f189d334c',
  projectTokenName: '7447696d62616c',
  contractTokenPolicyID:
    '1d046cbf9ee3af9ffe10c3d7820155dce444cdbeec75e67763dec223',
};
