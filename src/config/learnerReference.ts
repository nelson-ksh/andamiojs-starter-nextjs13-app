import learnerReferenceValidator from '../cardano/plutus/learnerReference-validator.json';

export const learnerReference = {
  address: 'addr_test1wqrg0fyzajjmk7e7tzystwk54h9zjuug7pgqt6wvv66389q60q7av',
  cbor: learnerReferenceValidator.cborHex,
  referenceUTxOAddress:
    'addr_test1vpvzkx5axre0wccg3lfpq75j3q005sxdsmzw2cuzjj6ca4qdz3yz3',
  referenceTxHash:
    'cf94f07c1f2ddf8772c7c73449c20216e655cdab5395487b4a3aac20f24d8db9',
  referenceTxIx: 1,
  referenceTxLovelace: '18907970',
};
