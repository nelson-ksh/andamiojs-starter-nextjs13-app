import { AndamioConfig } from 'andamiojs';
import { assignment } from './assignment';
import { contributorMinter, contributorReference } from './contributor';
import { courseReference } from './courseReference';
import { escrows } from './escrow';
import { learnerReference } from './learnerReference';
import { tokens } from './tokens';
import { treasury } from './treasury';

export const andamioConfig: AndamioConfig = {
  title: 'Andamio Plutus Optimized Spending Validators',
  baseAddress: '',
  enterpriseAddress: '',
  rewardAddress: '',
  // at the moment, each contract has a different reference UTxO Address - see individual contract configs
  // referenceScriptAddress: '',
  metadataKey: '',
  network: '0',
  tokens: tokens,
  treasury: treasury,
  escrows: escrows,
  contributorReference: contributorReference,
  contributorMinter: contributorMinter,
  courseReference: courseReference,
  learnerReference: learnerReference,
  assignment: assignment,
};
