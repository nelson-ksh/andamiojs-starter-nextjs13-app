import assignmentValidator from '../cardano/plutus/assignment-validator.json';

export const assignment = {
  address: 'addr_test1wqcydm8hgx2k3r0nkc36rpxp5m4r0xms8jcuuatmap65y6qkhskcf',
  cbor: assignmentValidator.cborHex,
  referenceUTxOAddress:
    'addr_test1vqhe8yz0pznn42aggykrjd0j8v0n4se65a64wvt0xpm459qw20yns',
  referenceTxHash:
    'cf94f07c1f2ddf8772c7c73449c20216e655cdab5395487b4a3aac20f24d8db9',
  referenceTxIx: 2,
  referenceTxLovelace: '19696700',
};
