import courseReferenceValidator from '../cardano/plutus/courseReference-validator.json';

export const courseReference = {
  address: 'addr_test1wrvrd4lyve4puyx8stdw0wq6aaz54ury5v2tx6ynf93wxlc23n765',
  cbor: courseReferenceValidator.cborHex,
  referenceUTxOAddress:
    'addr_test1vqv8zykkznqsnzuyu3e5jt7tp7n2vwnqelugfjh7y90ncccw63r5k',
  referenceTxHash:
    'cf94f07c1f2ddf8772c7c73449c20216e655cdab5395487b4a3aac20f24d8db9',
  referenceTxIx: 0,
  referenceTxLovelace: '15313430',
};
