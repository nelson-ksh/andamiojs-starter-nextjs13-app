import {
  escrowByContractToken,
  hexToString,
  posixToDateString,
  queryTreasuryInfo,
} from "andamiojs";
import { andamioConfig } from "../../../config";
import { Suspense } from "react";
import Loading from "../../Loading";
import Link from "next/link";

const ProjectsTable = async () => {
  const TreasuryInfo = await queryTreasuryInfo(andamioConfig);
  return (
    <tbody>
      {TreasuryInfo.data.map((project, index) => {
        const projectString = JSON.stringify(project);
        return (
          <tr
            key={index}
            className={index % 2 === 0 ? "bg-gray-600" : "bg-gray-700"}
          >
            <th className="px-6 py-4">{index + 1}</th>
            <td className="px-6 py-4">{hexToString(project.projectHashId)}</td>
            <td className="px-6 py-4">
              {escrowByContractToken(project.contractToken, andamioConfig).name}
            </td>
            <td className="px-6 py-4">
              {posixToDateString(project.expirationTime)}
            </td>
            <td className="px-6 py-4">{project.lovelaceAmount / 1000000}</td>
            <td className="px-6 py-4">{project.projectTokenAmount}</td>
            <td className="px-6 py-4">
              <Link
                href={`/contributor-platform/approved-projects/commit/${encodeURIComponent(
                  projectString
                )}`}
                className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
              >
                Commit
              </Link>
            </td>
          </tr>
        );
      })}
    </tbody>
  );
};

export default async function ApprovedProjects() {
  const projectsTable = await ProjectsTable();
  return (
    <main className="flex flex-col items-center justify-center font-semibold">
      <div className="mt-20">
        {
          <Suspense fallback={<Loading />}>
            <table className="w-full border-collapse border border-gray-400 mb-40">
              <thead>
                <tr className="bg-gray-800 text-white">
                  <th className="px-6 py-3 text-left">#</th>
                  <th className="px-6 py-3 text-left">Project</th>
                  <th className="px-6 py-3 text-left">Escrow</th>
                  <th className="px-6 py-3 text-left">Expiration</th>
                  <th className="px-6 py-3 text-left">ADA</th>
                  <th className="px-6 py-3 text-left">tGimbals</th>
                  <th className="px-6 py-3 text-left"></th>
                </tr>
              </thead>
              {projectsTable}
            </table>
          </Suspense>
        }
      </div>
    </main>
  );
}
