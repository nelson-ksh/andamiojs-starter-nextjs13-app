"use client";

import { useWallet } from "@meshsdk/react";
import {
  ProjectData,
  escrowByContractToken,
  hexToString,
  posixToDateString,
  prepareCommitTx,
  prepareDistributeTx,
} from "andamiojs";
import { andamioConfig } from "../../../../../../config";

export default function DistributeTransaction({
  params,
}: {
  params: { projectString: string };
}) {
  const { wallet, connected } = useWallet();
  const project: ProjectData = JSON.parse(
    decodeURIComponent(params.projectString)
  );
  async function onClick() {
    try {
      const DISTRIBUTE_TX = await prepareDistributeTx(
        wallet,
        project,
        andamioConfig
      );
      const res = await DISTRIBUTE_TX.runTx();
      console.log(res);
      return res;
    } catch (error) {
      console.log("Error2");
      throw error;
    }
  }
  return (
    <main className="flex flex-col items-center mt-10">
      {connected ? (
        <div className="flex flex-col py-10 items-center bg-gradient-br w-max p-24">
          <div className="font-extrabold mb-4 text-xl">Selected Project</div>
          <div>
            <table>
              <tbody>
                <tr>
                  <td className="font-bold">
                    <text className="mr-10">Project:</text>
                  </td>
                  <td>{hexToString(project.projectHashId).toUpperCase()}</td>
                </tr>
                <tr>
                  <td className="font-bold">Expires:</td>
                  <td>{posixToDateString(project.expirationTime)}</td>
                </tr>
                <tr>
                  <td className="font-bold">ADA:</td>
                  <td>{project.lovelaceAmount / 1000000}</td>
                </tr>
                <tr>
                  <td className="font-bold">tGimbals:</td>
                  <td>{project.projectTokenAmount}</td>
                </tr>
                <tr>
                  <td className="font-bold">Escrow:</td>
                  <td>
                    {escrowByContractToken(
                      project.contractToken,
                      andamioConfig
                    ).name.toUpperCase()}
                  </td>
                </tr>
                <tr>
                  <td className="font-bold">Contributor:</td>
                  <td>{hexToString(project.contributorAsset!.slice(56))}</td>
                </tr>
              </tbody>
            </table>
          </div>
          <button
            onClick={onClick}
            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded w-fit mt-6"
            type="button"
          >
            Confirm Distribute
          </button>
        </div>
      ) : (
        <div>Connnect to wallet</div>
      )}
    </main>
  );
}
