import { Suspense } from "react";
import { HeroGrid, HeroSection } from "../../../../components/ui/Hero";
import Loading from "../../../Loading";
import { andamioConfig } from "../../../../config";
import EscrowInfo from "../../EscrowInfo";

export default async function EscrowStat({
  params,
}: {
  params: { contractToken: string };
}) {
  const EscrowHero = await EscrowInfo(params.contractToken);
  return (
    <div className="flex flex-col py-10 items-center bg-gradient-tl mt-1 w-3/4 mx-auto">
      <div className="font-extrabold text-4xl mb-8">
        {
          andamioConfig.escrows.find(
            (escrow) => escrow.contractTokenName === params.contractToken
          )?.name.toUpperCase()
        }
      </div>
      <div className="mx-auto w-3/4">
        <HeroGrid>
          <Suspense fallback={<Loading />}>{EscrowHero}</Suspense>
        </HeroGrid>
      </div>
    </div>
  );
}
