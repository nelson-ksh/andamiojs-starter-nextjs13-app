"use client";

import { useFormik } from "formik";
import { useState } from "react";
import MintContribToken from "../../../components/transactions/MintContribToken";

export default function MintContribPage() {
  const [contribTokenName, setContribTokenName] = useState<string | undefined>(
    undefined
  );

  const formik = useFormik({
    initialValues: {
      contribAlias: "",
    },
    onSubmit: (values) => {
      console.log(values);
      setContribTokenName("Andamio-Contrib-" + values.contribAlias);
    },
  });

  return (
    <main className="flex flex-col items-center justify-center h-max">
      <div className="flex flex-col py-10 items-center bg-gradient-tl min-w-full mt-1">
        <h1>MINT A CONTRIBUTOR TOKEN</h1>
        {contribTokenName && <MintContribToken tokenAlias={contribTokenName} />}
      </div>

      <form onSubmit={formik.handleSubmit}>
        <div className="flex flex-col gap-3 text-white my-5">
          <label htmlFor="contribAlias">Enter an Alias</label>
          <input
            id="contribAlias"
            name="contribAlias"
            type="text"
            onChange={formik.handleChange}
            value={formik.values.contribAlias}
            className="text-indigo-700 p-3 rounded-lg"
          />
        </div>
        <button>Submit</button>
      </form>
    </main>
  );
}
