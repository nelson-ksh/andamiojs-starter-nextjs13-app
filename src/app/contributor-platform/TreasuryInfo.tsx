import { queryTreasuryInfo } from "andamiojs";
import { DataBox } from "../../components/ui/DataBox";
import { andamioConfig } from "../../config";
import React from "react";

const TreasuryInfo = async () => {
  const TreasuryInfo = await queryTreasuryInfo(andamioConfig);
  return (
    <>
      <DataBox value={TreasuryInfo.totalLovelace / 1000000} label="Total ADA" />
      <DataBox value={TreasuryInfo.totalTokens} label="Total tGimbals" />
      <DataBox value={TreasuryInfo.numFundUTxOs} label="Fund UTxOs" />
      <DataBox
        value={TreasuryInfo.numContractTokenUTxOs}
        label="Contract Token UTxOs"
      />
    </>
  );
};

export default TreasuryInfo;
