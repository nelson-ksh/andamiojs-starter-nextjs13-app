import {
  numCompletedCommitments,
  queryContribRefInfo,
  queryContribRefUTxOs,
} from "andamiojs";
import { DataBox } from "../../components/ui/DataBox";
import { HeroBasic, HeroGrid, HeroSection } from "../../components/ui/Hero";
import Link from "next/link";
import { andamioConfig } from "../../config";

const ContribRefInfo = async () => {
  const ContribRefUTxOs = await queryContribRefInfo(andamioConfig);
  return (
    <>
      <DataBox value={ContribRefUTxOs.utxos.length} label="Contributors" />
      <DataBox
        value={ContribRefUTxOs.numCompletedCommitments}
        label="Completed Commitments"
      />
    </>
  );
};

export default ContribRefInfo;
